// This code will help us access contents of express module/package
	// A "module" is a software component or part of a program that contains one or more routines
// It also allows us to access methods and functions that we will use to easily create an app/server
// We store oure express module to variable so we could easily access its keywords, functions, and methods.
const express = require("express");

// This code creates an application using express / a.k.a express application
	// App is our server
const app = express();

// For our applications erver to run, we need a port to listen to
const port = 3000;

// For our application could read json data 
app.use(express.json());

// Allows your app to read data from forms
// By default, information received from the url can only be received as string or an array
// By applyhing the option of "extended:true" this allows us to receive information in other data types, such as an object which we will use throughout our application
app.use(express.urlencoded({extended:true}));

// This route expects to receive a GET request at the URI/endpoint "/hello"
app.get("/hello", (request, response)=>{

	// This is the response that we will expect to receive if the get method with the right endpoint is successful 
	response.send("GET method success. \nHello from /hello endpoint!");

});

app.post("/hello", (request, response) => {
	response.send(`Hello there ${request.body.firstName} ${request.body.lastName}!`);
});

let users = [];

app.post("/signup", (request, response) => {
	if(request.body.username !== "" && request.body.password !== ""){
		users.push(request.body);
		console.log(users);
		response.send(`User ${request.body.username} succesfully registered`);
    //sending another reponnse will result an error, only the first response will display
    response.send(`Welcome ${request.body.username}`);
	}
	else{
		response.send("Please input BOTH username and password");
	}

});
//Syntax:
// app.httpMethod("/endpoint", (request, response) => {
//   //code block
// });

app.put("/change-password", (request, response) => {
  
  // We create a variablle where we will store our response
  let message;

  // initialization //condition// change of value // index
  for(let i=0; i< users.length; i++){

    //We check if the user is existing in our users array
    // We check if the user is existing in our users array
    if(request.body.username == users[i].username){
      //update an element objects password based on the input on the body
      users[i].pasword = request.body.password

      //We reassign what message we would like to recieve as response 
      message =  `User ${request.body.username}'s password has been updated`
      break;
    }
    else{
      //if there is no match we would like to receive a message that user does not exist
      message =  `User does not exist.`
    };
  }
  // We display updated array
  console.log(users)
  //We display our response if there is a match(Successfully updated the password) or if (User does not exist)
  response.send(message);
});


//1.
app.get("/home", (request, response)=>{


	response.send("Welcome to the homepage");

});
//2.
app.get("/users", (request, response) => {
	response.send(users);
});

//3.
app.delete("/delete-user", (request, response) => {
  let message;
  for(let i=0; i< users.length; i++){

    if(request.body.username == users[i].username){
      users[i].username = request.body.username
      message =  "User deleted"
      users.splice(i, 1)
      break;
    }
    else{
      message =  `User does not exist.`
    };
  }
  console.log(users)
  response.send(message);
});


// Tells our server/application to listen to the port
// If the port is accessed, we can run the server
// Returns a message to confirm that the server is running in the terminal
app.listen(port, () => console.log(`Server running at port ${port}`))





